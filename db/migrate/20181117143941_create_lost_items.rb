class CreateLostItems < ActiveRecord::Migration[5.1]
  def change
    create_table :lost_items do |t|
      t.string :lid
      t.string :item_name
      t.text :detail
      t.datetime :date
      t.string :from
      t.timestamps
    end
  end
end
