class CreateFoundItems < ActiveRecord::Migration[5.1]
  def change
    create_table :found_items do |t|
      t.string :fid
      t.string :item_name
      t.text :detail
      t.datetime :date
      t.string :from
      t.timestamps
    end
  end
end
