Rails.application.routes.draw do
  get 'sessions/new'

  get 'static_pages/about'
  get 'static_pages/found'
  get 'static_pages/lost'
  get 'static_pages/login'
  get 'static_pages/signup'
  root 'static_pages#lost'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
